defmodule CLITest do
  use ExUnit.Case
  doctest Issues.CLI

  import Issues.CLI, only: [parse_args: 1, sort_in_descending_order: 1]

  test ":help is returned when -h or --help is passed" do
    assert parse_args(["-h", "anything"]) == :help
    assert parse_args(["--help", "anything"]) == :help
  end

  test "returns 3 values when 3 are provided" do
    assert parse_args(["user", "project", "99"]) == { "user", "project", 99 }
  end

  test "returns 3 values including a default when 2 are provided" do
    assert parse_args(["user", "project"]) == { "user", "project", 4 }
  end

  test "correctly sorts the list in descending order by created_at" do
    test_list = [
      %{"created_at" => 2},
      %{"created_at" => 1},
      %{"created_at" => 3}
    ]

    result = sort_in_descending_order(test_list)

    assert result == [
      %{"created_at" => 3},
      %{"created_at" => 2},
      %{"created_at" => 1}
    ]
  end

end
