defmodule Issues.CLI do
  @default_count 4
  @headers ["number", "created_at", "title"]

  import Issues.TableFormatter, only: [print_table_for_columns: 2]

  @moduledoc """
  Handle the command line parsing and dispatch to
  the various functions that end up generating a table
  of the last _n_ issues in a github project
  """

  def main(argv) do
    parse_args(argv)
    |> process
    |> print_table_for_columns(@headers)
  end

  def process(:help) do
    IO.puts """
    usage: issues <user> <project> [ count | #{@default_count} ]
    """

    System.halt(0)
  end

  def process({user, project, count}) do
    Issues.GithubIssues.fetch(user, project)
    |> sort_in_descending_order
    |> last(count)
  end


  @doc """
  `argv` can contain `-h` or `--help` in the first position, which returns `:help`

  Otherwise in the second position `argv` contains a list with a github user name,
  a project name, and optionally the number of entries to format

  Returns a tuple of `{ user, project, count}`, or `:help` if `--help`, `-h`or an invlaid argument was given
  """
  def parse_args(argv) do
    OptionParser.parse(
      argv,
      [
        switches: [ help: :boolean ],
        aliases: [ h: :help ]
      ]
    )
    |> elem(1)
    |> extract_args
  end

  # with count
  defp extract_args([user, project, count]) do
    {user, project, String.to_integer(count)}
  end

  # with `@default_count` applied
  defp extract_args([user, project]) do
    {user, project, @default_count}
  end

  # `-h`, `--help`, or invalid args
  defp extract_args(_) do
    :help
  end

  def sort_in_descending_order(list) do
    list
    |> Enum.sort(&(&1["created_at"] >= &2["created_at"]))
  end

  defp last(list, count) do
    list
    |>Enum.take(count)
  end

 end
