defmodule Issues.GithubIssues do
  @user_agent [ {"User-agent", "Elixir issues project"}]
  @github_url Application.get_env(:issues, :github_url)

  def fetch(user, project) do
    issues_url(user, project)
    |> HTTPoison.get(@user_agent)
    |> handle_response()
    |> return_body_or_error()
  end

  defp issues_url(user, project) do
    "#{@github_url}/repos/#{user}/#{project}/issues"
  end

  defp handle_response({ _, %{status_code: status_code, body: body} }) do
    {
      status_code |> check_for_error(),
      body |> Poison.Parser.parse!()
    }
  end

  defp check_for_error(200), do: :ok
  defp check_for_error(_), do: :error

  defp return_body_or_error({:ok, body}) do
    body
  end

  defp return_body_or_error({ :error, error}) do
    error
  end
end
